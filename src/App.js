import "./App.css";
import Award from "./asserts/1.png";
import Person from "./asserts/2.png";
import Catalog from "./asserts/3.png";

import Logo from "./asserts/logo.png";
import { FaFacebook, FaGlobe } from "react-icons/fa";
import { IoCallSharp } from "react-icons/io5";
import React, { useState } from "react";

function App() {
     const [products, setProducts] = useState([
          "CHEMICALS & PROCESS",
          "POWER",
          "WATER & WASTE WATER ",
          "OILS & GAS",
          "PHARMA",
          "SUGARS & DISTILLERIES",
          "PAPER & PULP",
          "MARINE & DEFENCE",
          "METAL & MINING",
          "FOOD & BEVERAGE",
          "PETROCHEMICAL REFINERIES",
          "SOLAR",
          "BUILDING",
          "HVAC",
          "FIRE FIGHTING",
          "BUILDING",
          "AGRICULTURE & RESIDENTIAL",
     ]);

     return (
          <div className="App">
               <div style={{ flex: 1, flexDirection: "row" }}>
                    <img src={Logo} alt="img1" style={{ width: "250px" }} />
               </div>
               <div
                    className="page1"
                    style={{ paddingBottom: "40px", flexWrap: "wrap" }}
               >
                    <div style={{ flex: 1, padding: "10px" }}>
                         <img
                              src={Award}
                              alt="img1"
                              style={{ width: "350px", height: "620px" }}
                         />
                    </div>
                    <div
                         style={{
                              flex: 2,
                              textAlign: "left",
                              margin: "0px 10px",
                         }}
                    >
                         <div>
                              <div style={{ margin: "0px 10px" }}>
                                   <b>
                                        C.R.I. PUMPS WINS THE NATIONAL ENERGY
                                        CONSERVATION AWARD 2018 for the 4th
                                        time.{" "}
                                   </b>
                                   <ul>
                                        <li>
                                             C.R.I.'s energy efficient products
                                             are well recognized by various
                                             Government Institutions, as
                                             trustworthy products for various
                                             projects across the globe to save
                                             energy.
                                        </li>
                                        <li>
                                             {" "}
                                             C.R.I. is the highest contributor
                                             in the country for the projects of
                                             EESL (Energy Efficiency Services
                                             Limited) to replace the old
                                             inefficient pumps with 5 Star rated
                                             energy efficient smart pumps with
                                             IoT enabled control panel.{" "}
                                        </li>
                                   </ul>
                              </div>
                         </div>
                         <div>
                              <img
                                   src={Person}
                                   alt="img2"
                                   style={{ width: "80%" }}
                              />
                         </div>
                         <div>
                              Government of India has awarded the "National
                              Energy Conservation Award 2018". Mr. G. Selvaraj,
                              Joint Managing Director of C.R.I. Group received
                              the award from Smt. Sumitra Mahajan, Speaker of
                              Lok Sabha & Shri. Raj Kumar Singh, Honorable
                              Minister of State.
                         </div>
                    </div>
               </div>

               <div
                    className="page2"
                    style={{ flexDirection: "column", margin: "0px 10px" }}
               >
                    <div>
                         INSTALLED OVER 10 LAKHS STAR RATED PUMPSETS ACROSS THE
                         COUNTRY RESULTING IN A CUMULATIVE SAVING OF MORE THAN
                         9,000 MILLION UNITS OF POWER FOR THE NATION.
                    </div>

                    <div>
                         <img
                              src={Catalog}
                              alt="img2"
                              style={{ width: "80%" }}
                         />
                    </div>
                    <div>
                         Valves - Pumps - Pipes - IoT Drives & Controllers -
                         Wires & Cables - Solar Systems - Motors
                    </div>
                    <div style={{ padding: "20px 0" }}>
                         <hr style={{ color: "red", width: "96%" }}></hr>
                    </div>
                    <div>
                         <b>
                              C.R.I. FLUID SYSTEMS PRODUCTS CATER TO DIVERSE
                              SEGMENTS
                         </b>
                    </div>

                    <div style={{ padding: "30px 0" }}>
                         {products.map((item) => {
                              return (
                                   <p style={{ display: "inline" }}>
                                        {item}
                                        <p
                                             style={{
                                                  display: "inline",
                                                  color: "red",
                                             }}
                                        >
                                             {" "}
                                             |{" "}
                                        </p>
                                   </p>
                              );
                         })}
                    </div>
               </div>
               <div className="footer">
                    <div className="footer-componente">
                         <IoCallSharp color="white" size="25px" />
                         <a
                              href="#"
                              style={{
                                   color: "white",
                                   textDecoration: "none",
                                   padding: "0px 5px",
                              }}
                         >
                              Toll Free 1800 200 1234
                         </a>
                    </div>
                    <div className="footer-componente2">
                         <FaFacebook color="white" size="25px" />
                         <a
                              href="#"
                              style={{
                                   color: "white",
                                   textDecoration: "none",
                                   padding: "0px 5px",
                              }}
                         >
                              www.facebook.com/cripumps
                         </a>
                    </div>
                    <div className="footer-componente3">
                         <FaGlobe color="white" size="25px" />
                         <a
                              href="#"
                              style={{
                                   color: "white",
                                   textDecoration: "none",
                                   padding: "0px 5px",
                              }}
                         >
                              www.crigroups.com
                         </a>
                    </div>
               </div>
          </div>
     );
}

export default App;
